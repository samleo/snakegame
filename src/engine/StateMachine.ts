export default class StateMachine
{
	private state: number;
	constructor ()
	{
		this.state = -1;
	}

	getState (  ): number
	{
		return this.state;
	}

	setState ( newState: number ): void
	{
		this.state = newState;
	}

	updateInput ( event: any ): void
	{

	}
	
	updateState ( game: Phaser.Game ): number
	{
		return this.getState();
	}
}