import Events from '../engine/Events';
import Marker from '../gui/Marker';
import Button from '../gui/Button';

export default class Options extends Phaser.State
{
	maxApples: number;
	constructor(game)
	{
		super();
		this.maxApples = game["maxApples"];
	}

	preload(game)
	{

	}

	create(game: Phaser.Game)
	{
		let main = new Phaser.BitmapText(game, game.world.centerX, game.world.centerY, game["fontName"]);
		let background = game.add.image(0,0,"backgroundMainMenu");
		let applesOption = new Marker({game:game, x:0, y:0, key:null, 
			name: "applesOption", normal: "markerOptionNormal", selected: "markerOptionSelected"});
		let lineTextApple = new Phaser.BitmapText(game, 0, 0, game["fontName"], "Infinity Apples Mode", 30);
		let backButton = new Button({game:game, x:0, y:0, key:"mainButtonBG", text: "Back to Main Menu", font:{key:game["fontName"], size: 30}});
		let rocksOption = new Marker({game:game, x:0, y:0, key:null, 
			name: "rocksOption", normal: "markerOptionNormal", selected: "markerOptionSelected"});
		let lineTextRock = new Phaser.BitmapText(game, 0, 0, game["fontName"], "With Rocks ", 30);

		background.width = game.width;
		background.height = game.height;
		
		rocksOption.y = applesOption.y - applesOption.height * 2 - 10;
		lineTextRock.y = rocksOption.y + rocksOption.height / 2;
		rocksOption.x = lineTextRock.x + lineTextRock.width + 10;
		applesOption.x = lineTextApple.x + lineTextApple.width + 10;
		lineTextApple.y = applesOption.height / 2;
		backButton.y = applesOption.y + applesOption.height * 1.5 + 10;
		backButton.x += backButton.width / 4;

		if (rocksOption.x > applesOption.x)
			applesOption.x = rocksOption.x;
		else
			rocksOption.x = applesOption.x;
		
		rocksOption.select(game["wall"]);
		
		backButton.onInputUp.add(function()
		{
			game.state.start("MainMenu");
		});

		if (game["maxApples"] != this.maxApples)
			applesOption.select(true);

		main.addChild(lineTextRock);
		main.addChild(rocksOption);
		main.addChild(lineTextApple);
		main.addChild(applesOption);
		main.addChild(backButton);
		main.x = main.x - main.width / 2;
		this.add.existing(main);

		Events.instance().on("marker.select.true", function(marker, that)
		{
			if (marker.name == "applesOption")
				game["maxApples"] = Math.floor(game.width / (game["cellSize"] - 1)) * Math.floor(game.height / (game["cellSize"] - 1));
			
			if (marker.name == "rocksOption")
				game["wall"] = true;
		}, this);

		Events.instance().on("marker.select.false", function(marker, that)
		{
			if (marker.name == "applesOption")
				game["maxApples"] = that.maxApples;
			
			if (marker.name == "rocksOption")
				game["wall"] = false;
		}, this);
	}

	shutdown(game)
	{
		Events.instance().destroy();
		super.shutdown(game);
	}
}
