import GameScreen from "./scenes/GameScreen";
import Boot from './scenes/Boot';
import PreloadState from "./scenes/PreloadState";
import MainMenu from './scenes/MainMenu';
import { resize } from './scenes/ScreenUtil';
import Options from './scenes/Options';

let game: Phaser.Game;
window.onload = function()
{
	game = new Phaser.Game(800, 608, Phaser.AUTO, "parent-game");
	game["maxApples"] = 5;//max apples
	game["wall"] = true;//wall around map
	game["cellSize"] = 32;//cell movement size in pixels
	game["fontName"] = "arial";//font name in game, not change this

	game.state.add("Boot", new Boot(), true);
	game.state.add("PreloadState", new PreloadState());
	game.state.add("MainMenu", new MainMenu());
	game.state.add("GameScreen", new GameScreen());
	game.state.add("Options", new Options(game));
}

window.addEventListener("resize", function()
{
	if (game)
		resize(game);
});
