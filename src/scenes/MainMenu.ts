import Button from '../gui/Button';
import Events from '../engine/Events';

export default class MainMenu extends Phaser.State
{
	main: Phaser.Sprite;
	continueGame: Button;
	startGame: Button;
	options: Button;

	constructor ()
	{
		super();
		this.key = "MainMenu";
	}

	preload(game: Phaser.Game): void
	{
		
	}

	create(game: Phaser.Game): void
	{
		let background = game.add.image(0,0,"backgroundMainMenu");
		background.width= game.width;
		background.height = game.height;

		let that = this;
		this.main = new Phaser.Sprite(game, 0, 0, null);
		/*
		this.continueGame = new Button({game:game, x:0, y:0, key: "mainButtonBG", name: "continue"});
		this.continueGame.onInputUp.add(function()
		{
			Events.instance().emit("mainmenu.click.continue", that);
			game.state.start("GameScreen", false, false);
		});*/

		this.startGame = new Button({game:game, x:0, y:0, key: "mainButtonBG", 
			name: "startGame", text:"Start Game", font:{key:game["fontName"], size: 30}});
		this.startGame.onInputUp.add(function()
		{
			Events.instance().emit("mainmenu.click.startGame", that);
			game.state.start("GameScreen");
		});

		this.options = new Button({game:game, x:0, y:0, key: "mainButtonBG", 
			name: "options", text: "Options", font:{key:game["fontName"], size: 30}});
		this.options.onInputUp.add(function()
		{
			game.state.start("Options");
			Events.instance().emit("mainmenu.click.options", that);
		});

		//this.continueGame.y = 
		this.startGame.y = /*this.continueGame.height*/ + 5;
		this.options.y = this.startGame.y + this.startGame.height + 5;

		//this.main.addChild(this.continueGame);
		this.main.addChild(this.startGame);
		this.main.addChild(this.options);
		this.main.anchor.set(0,0);
		this.add.existing(this.main);

		if (game["scores"])
		{
			let scores = new Phaser.BitmapText(game, 0, 0, game["fontName"], "Best Score: " + game["scores"], 50);
			scores.anchor.set(0,0);
			scores.y = this.startGame.y - 1.5*this.options.height;
			scores.x = (-scores.width + this.options.width) / 2;
			this.main.addChild(scores);
		}

		this.main.x = game.world.centerX - this.options.width / 2;
		//this.main.y = game.world.centerY + (this.continueGame.y - (this.options.y + this.options.height)) / 2;
		this.main.y = game.world.centerY + (this.startGame.y - (this.options.y + this.options.height)) / 2;
	}

	shutdown(game)
	{
		Events.instance().destroy();
		super.shutdown(game);
	}
}