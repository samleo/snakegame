/// <reference path="../../definitions/phaser.d.ts" />
//aqui button
export default class Button extends Phaser.Button
{
	DIFF_Y: number = -4;
	DIFF_X: number = -2;
	protected oldTint: number;
	protected pressTint: number;
	protected value: number;
	protected text: Phaser.BitmapText;
	

	constructor ( config: any )
	{
		super(config.game, config.x, config.y, config.key);
		this.game.add.existing(this);
		this.name = config.name;
		let that= this;
		if (config.width)
			this.width = config.width;
		if (config.height)
			this.height = config.height;
		
		if (config.over)
			this.onInputOver.add(config.over, this); 
		
			if (config.up)
		{
			this.onInputUp.add(config.up,this);
		}

		if (config.down)
		{
			this.onInputDown.add(config.down,this);
		}
		
		if (config.up)
			this.onInputOut.add(config.out,this);
	
		this.onInputUp.add(()=>{that.unpress();}, this);
		this.onInputDown.add(()=>{that.press();},this);
		
		if (config.text)
		{
			this.text = this.game.add.bitmapText(0, 0, config.font.key, config.text, config.font.size);
			this.text.anchor.set(0,0);
			this.addChild(this.text);
			this.centerObject(this.text);
		}
		
		if (config.value == null)
			config.value = " ";
		else
			this.setValue(config.value);
		
		if (config.pressTint)
			this.pressTint = config.pressTint;
		else
			this.pressTint = ~(this.tint & 0xFFFFFF);
		this.oldTint = this.tint;
	}

	press(): void
	{
		this.tint = this.pressTint;
	}

	unpress (): void
	{
		this.tint = this.oldTint;
	}

	centerObject (object: any): void
	{
		if (!object)
			return;
		
		let width = [this.width, object.width];
		if (width[0] > width[1]) 
			object.x = (width[0] - width[1]) / 2 + this.DIFF_X;
		else
		{
			object.x = this.DIFF_X;
			object.width = this.width;
		}

		let height = [this.height, object.height];
		if (height[0] > height[1]) 
			object.y = (height[0] - height[1]) / 2 + this.DIFF_Y;
		else
		{
			object.height = height[0];
			object.y = this.DIFF_Y;
		}
	}

	setValue ( value: number ): void
	{
		if (this.text)
		{
			this.text.text = String(value);
			this.centerObject(this.text);
		}
		this.value = value;
	}

	getValue (): number
	{
		return this.value;
	}
}