//tirado daqui: https://gist.github.com/eguneys/5cf315287f9fbf413769

export default class Gesture 
{
	static SWIPE = 0;
	static TAP = 1;
	static HOLD = 2;

	static TIMES = {
			HOLD: 150,
			SWIPE: 250
	};

	game: Phaser.Game;
	swipeDispatched: boolean;
	holdDispatched: boolean;

	isTouching: boolean;
	isHolding: boolean;

	sense: Phaser.Point;
	oldPos: Phaser.Point;

	onSwipe: Phaser.Signal;
	onTap: Phaser.Signal;
	onHold: Phaser.Signal;

	constructor (game)
	{
		this.game = game;

		this.swipeDispatched = false;
		this.holdDispatched = false;

		this.isTouching = false;
		this.isHolding = false;

		this.sense = new Phaser.Point(0,0);
		this.oldPos = new Phaser.Point(0,0);

		this.onSwipe = new Phaser.Signal();
		this.onTap = new Phaser.Signal();
		this.onHold = new Phaser.Signal();
	}

	update()
	{
		var distance = Phaser.Point.distance(this.game.input.activePointer.position, this.game.input.activePointer.positionDown);
		var duration = this.game.input.activePointer.duration;

		this.updateSwipe(distance, duration);
		this.updateTouch(distance, duration);

		if (this.game.input.activePointer.isDown)
		{
			this.sense.x = this.game.input.activePointer.position.x - this.oldPos.x;
			this.sense.y = this.game.input.activePointer.position.y - this.oldPos.y;
		}
		this.oldPos.set(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y);
	}

	updateSwipe(distance, duration)
	{
		if (duration === -1) {
				this.swipeDispatched = false;
		} else if (!this.swipeDispatched && distance > 150 &&  duration > 100 && duration < Gesture.TIMES.SWIPE) {
				var positionDown = this.game.input.activePointer.positionDown;
				this.onSwipe.dispatch(this, positionDown);

				this.swipeDispatched = true;
		}
	}

	updateTouch (distance, duration)
	{
		var positionDown = this.game.input.activePointer.positionDown;

		if (duration === -1) {
								if (this.isTouching) {
						this.onTap.dispatch(this, positionDown);
				}

				this.isTouching = false;
				this.isHolding = false;
				this.holdDispatched = false;

		} else if (distance < 10) {
				if (duration < Gesture.TIMES.HOLD) {
						this.isTouching = true;
				} else {
						this.isTouching = false;
						this.isHolding = true;

						if (!this.holdDispatched) {
								this.holdDispatched = true;

								this.onHold.dispatch(this, positionDown);
						}
				}
		} else {
				this.isTouching = false;
				this.isHolding = false;
		}
	}
}