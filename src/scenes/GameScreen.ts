/**
 * Sinais emitidos:
 * "snake.body.growup"
 * "snake.start.game"
 * "snake.ate.body"
 */
/// <reference path="../../definitions/phaser.d.ts" />
import Events from '../engine/Events';
import Key from '../engine/Key';
import SnakeHUD from '../gui/SnakeHUD';
import Piece from '../objects/Piece';
import { Direction } from '../objects/Piece';
import Gesture from '../engine/Gesture';

export default class GameScreen extends Phaser.State 
{
	score: number;
	body: Array<Piece>;
	apples: Array<Piece>;
	eatSnake: boolean;
	mapWidth: number;
	mapHeight: number;
	mapStartW: number;
	mapStartH: number;
	pixelSize: number; // usado para posicionar as imagens antes de desenhar na tela
	vel: Phaser.Point; // 
	currSize: number;
	
	// na sequencia:  Direction.UP,Direction.DOWN,Direction.RIGHT,Direction.LEFT
	keys: Array<boolean> = [false,false,false,false];
	
	eatenPos: Array<Phaser.Point>;//pontos onde tem partes com maçãs comidas
	excludedArea: Array<Phaser.Rectangle>;//areas para excluir das maçãs cairem nelas
	
	//teclado
	phaserKeys: Array<Key>;
	pauseGame: boolean;

	//gestos
	gesture: Gesture;

	counter: number;
	inited: boolean = false;
	maxApples: number;

	rocks: Array<Piece>;

	constructor (  )
	{
		super();
		this.counter = 0;
		this.key = "GameScreen";
		this.body = Array<Piece>();
		this.apples = Array<Piece>();
		this.eatenPos = new Array<Phaser.Point>();
		this.excludedArea = new Array<Phaser.Rectangle>();
		this.vel = new Phaser.Point();
	}

	init (game: Phaser.Game)
	{
		
	}

	preload(game: Phaser.Game):void
	{
		
	}

	create(game: Phaser.Game): void
	{
		this.maxApples = game["maxApples"];
		this.score = 0;
		this.pixelSize = game["cellSize"];
		this.body = Array<Piece>();
		this.apples = Array<Piece>();
		this.eatenPos = new Array<Phaser.Point>();
		this.excludedArea = new Array<Phaser.Rectangle>();
		this.vel = new Phaser.Point();

		

		this.eatSnake = false;
		this.gesture = new Gesture(game);
		Gesture.TIMES.SWIPE = 150;
		//Gesture.TIMES.HOLD
		this.phaserKeys = Array<Key>();
		this.phaserKeys.push(new Key(game, Phaser.Keyboard.UP));
		this.phaserKeys.push(new Key(game, Phaser.Keyboard.DOWN));
		this.phaserKeys.push(new Key(game, Phaser.Keyboard.RIGHT));
		this.phaserKeys.push(new Key(game, Phaser.Keyboard.LEFT));
		
		Events.instance().on("key.state.free", this.keyboard, this);
		Events.instance().on("key.state.press", this.keyboard, this);
		Events.instance().on("key.state.hold", this.keyboard, this);
		Events.instance().on("key.state.release", this.keyboard, this);

		let bg = game.add.image(0, 0, "background");

		bg.width = game.width;
		bg.height = game.height;
		
		this.apples.push(new Piece(0,0,-1, {game:game, key:"apple", width:this.pixelSize, height: this.pixelSize}));

		
		
		Events.instance().on("mainmenu.click.continue", function(mainmenu, that)
		{
			if (game.state.current == "MainMenu")
			{
				that.pauseGame = false;
			}
		}, this);

		/*
		//GameState.setOrientation("portrait");
		if (GameState.getOrientationValue().equals("landscape"))
		{
			camera = new OrthographicCamera(1024,600);
			camera.setToOrtho(true, 1024,600);
			viewport = new StretchViewport(1024,600,camera);
			mapStartW =this.mapStartH = 0;
		}
		else
		{
			camera = new OrthographicCamera(600,1024);
			camera.setToOrtho(true, 600,1024);
			viewport = new StretchViewport(600,1024,camera);
			mapStartW =this.mapStartH = 0;
		}*/
		
		
		

		this.mapStartW = this.mapWidth = 0;
		this.mapStartH = this.mapHeight = 0;
		let addW = 0;
		let addH = 0;
		if (game.width % this.pixelSize > this.pixelSize/2)
		{
			addW++;
		}
		
		if (game.height % this.pixelSize > this.pixelSize/2)
		{
			addH++;
		}

		/*this.mapStartW = addW;
		this.mapStartH = addH;*/
		this.mapWidth = Math.floor(game.width/this.pixelSize) + addW;
		this.mapHeight = Math.floor(game.height/this.pixelSize) + addH;
		
		this.rocks = Array<Piece>();
		if (game["wall"])
			for (let i = 0; i < this.mapHeight; ++i)
				for (let j= 0; j < this.mapWidth; ++j)
					if ((i == 0 || i == this.mapHeight - 1) || (j == 0 || j == this.mapWidth - 1))
						this.rocks.push(new Piece(j, i, -1,
							{game:game, key:"rock", width: this.pixelSize, height: this.pixelSize}));

		let hud = new SnakeHUD({game:game,x:0,y:0, key:null, font:{key:game["fontName"], size:50}});
		Events.instance().on("snakehud.to.mainmenu", function(hud, that){
			that.pauseGame = true;
		}, this);

		console.log("mapW= "+this.mapWidth);
		console.log("mapH = "+this.mapHeight);

		

		this.startGame();
	}
	
	shutdown(game)
	{
		Events.instance().destroy();
		super.shutdown(game);
	}

	elapsed: number = 0;
	dest: Phaser.Point;
	update ( )
	{
		if (this.pauseGame)
		{
			return;
		}
		Events.instance().emit('snake.game.run', this);
		this.gesture.update();
		
		/*if (gui.isPaused() == false) */
		{
			//atualiza as teclas
			for (let key of this.phaserKeys)
			{
				key.updateInput(null);
				key.updateState();
			}

			this.elapsed += this.game.time.elapsed / 1000;
			if (this.elapsed > 0.200)
			{
				this.elapsed = 0;
				
				this.moveSnake();
				
				let lock = false;
				// colisão com a maçã
				for (var i = 0; i < this.apples.length; i++)
				{
					let p = this.apples[i];
					if ((this.body[this.body.length - 1].pos.x == p.pos.x) && (this.body[this.body.length - 1].pos.y == p.pos.y))
					{
						this.eatenPos.push(new Phaser.Point(this.body[this.body.length - 1].pos.x,
							this.body[this.body.length - 1].pos.y));
						this.body.push(new Piece(this.body[this.body.length - 1].pos.x, this.body[this.body.length - 1].pos.y, 
							this.body[this.body.length - 1].dir, 
							{
								game:this.game, key: "apple",
								width: this.pixelSize, height: this.pixelSize
							}));
						Events.instance().emit("snake.body.growup", this);
						this.moveSnake();
						// reinicializando a posição da maçã
						// escolhe uma posição diferente das peças da serpente
						this.setApplePos(p);
						i = 0;
						
						// adiciona mais maçãs se possivel
						if (lock == false && Math.random() < 0.5 && this.apples.length > 0 && this.apples.length < this.mapWidth*this.mapHeight*0.1)
						{
							lock = true;//evita ficar readicionando maçãs
							if (this.apples.length >= this.maxApples)
								continue;
							
							this.apples.push(new Piece(0,0,-1, {game: this.game, key:"apple", width:this.pixelSize, height: this.pixelSize}));
							let equals = false;
							do
							{
								equals = false;

								this.setApplePos(this.apples[this.apples.length - 1]);
								for (let q of this.apples)
								{
									if (q != this.apples[this.apples.length - 1])
										if (q.pos.x == this.apples[this.apples.length - 1].pos.x &&
										    q.pos.y == this.apples[this.apples.length - 1].pos.y)
										{
											equals = true;
											break;
										}
								}
							} while(equals);
							
							if ((this.body[this.body.length - 1].pos.x == this.apples[this.apples.length - 1].pos.x) && (this.body[this.body.length - 1].pos.y == this.apples[this.apples.length - 1].pos.y))
							{
								this.eatenPos.push(new Phaser.Point(this.body[this.body.length - 1].pos.x,
									this.body[this.body.length - 1].pos.y));
								Events.instance().emit("snake.body.growup", this);
								this.body.push(new Piece(this.body[this.body.length - 1].pos.x, this.body[this.body.length - 1].pos.y, 
									this.body[this.body.length - 1].dir, 
									{
										game:this.game, key:"apple", 
										width:this.pixelSize, height: this.pixelSize
									}));
								this.moveSnake();
								this.setApplePos(this.apples[this.apples.length - 1]);
							}
						}
					}
				}
				
				//remove o bucho cheio
				for (let i = 0; i < this.eatenPos.length; i++)
				{
					if ((this.body[0].pos.x == this.eatenPos[i].x && this.body[0].pos.y == this.eatenPos[i].y) ||
					     (!this.hasBodyPiece(this.eatenPos[i].x,this.eatenPos[i].y-1)&&
					      !this.hasBodyPiece(this.eatenPos[i].x,this.eatenPos[i].y+1)&&
					      !this.hasBodyPiece(this.eatenPos[i].x-1,this.eatenPos[i].y)&&
					      !this.hasBodyPiece(this.eatenPos[i].x+1,this.eatenPos[i].y)))
					{
						this.eatenPos.splice(i, 1);
						i = 0;
						if (this.eatenPos.length == 0)
							break;
					}
				}
				
				if (this.eatSnake || this.eatenRock())
				{
					while(this.apples.length > 1)
						this.apples.pop().destroy();
					let aux: Piece = new Piece(0,0,-1,
						{
							game:this.game, key:"apple", 
							width:this.pixelSize, height: this.pixelSize
						});
					this.setApplePos(aux);
					this.apples.push(aux);
					
					this.eatSnake = false;
					while (this.eatenPos.length > 0)
						this.eatenPos.pop();
						Events.instance().emit("snake.ate.body", this);
					this.startGame();
				}

				this.updateBody();
			}
		}
		// verifica se mordeu uma parte do corpo
		for (var i = 0; i < this.body.length - 2 && this.eatSnake == false; i++)
		{
			if (this.body[this.body.length - 1].pos.x ==this.body[i].pos.x && this.body[this.body.length - 1].pos.y ==this.body[i].pos.y)
				this.eatSnake = true;
		}

		
	}
	
	reset (  ) {
		
	}
	
	hasBodyPiece ( x: number, y: number ): boolean
	{
		for (let p of this.body)
			if (p.pos.x == x && p.pos.y == y)
				return true;
		return false;
	}

	eatenRock (  ): boolean
	{
		for (let rock of this.rocks)
		{
			if (rock.pos.x == this.body[this.body.length - 1].pos.x &&
					rock.pos.y == this.body[this.body.length - 1].pos.y)
				return true;
		}

		return false;
	}

	moveSnakeOnSwipe (  )
	{
		if (Math.abs(this.gesture.sense.x) > Math.abs(this.gesture.sense.y))
		{
			if (this.gesture.sense.x > 0)
			{
				this.keys[Direction.UP] = false;
				this.keys[Direction.DOWN] = false;
				this.keys[Direction.RIGHT] = true;
				this.keys[Direction.LEFT] = false;
			}
			else
			{
				this.keys[Direction.UP] = false;
				this.keys[Direction.DOWN] = false;
				this.keys[Direction.RIGHT] = false;
				this.keys[Direction.LEFT] = true;
			}
		}
		else if (Math.abs(this.gesture.sense.x) < Math.abs(this.gesture.sense.y))
		{
			if (this.gesture.sense.y > 0)
			{
				this.keys[Direction.UP] = false;
				this.keys[Direction.DOWN] = true;
				this.keys[Direction.RIGHT] = false;
				this.keys[Direction.LEFT] = false;
			}
			else
			{
				this.keys[Direction.UP] = true;
				this.keys[Direction.DOWN] = false;
				this.keys[Direction.RIGHT] = false;
				this.keys[Direction.LEFT] = false;
			}
		}
	}
	
	moveSnake (  )
	{
		//tenta mover pelo swipe
		this.moveSnakeOnSwipe();

		if (this.keys[Direction.RIGHT] && this.body[this.body.length - 1].dir != Direction.LEFT)
		{
			this.vel.x = 1; // move horizontalmente a cabeça para direita
			this.vel.y = 0; // e para de mover verticalmente
			this.body[this.body.length - 1].dir = Direction.RIGHT;
		}
		else if (this.keys[Direction.LEFT] && this.body[this.body.length - 1].dir != Direction.RIGHT)
		{
			this.vel.x = -1;  // move horizontalmente a cabeça para esquerda
			this.vel.y = 0; // e para de mover verticalmente
			this.body[this.body.length - 1].dir = Direction.LEFT;
		}
		else if (this.keys[Direction.UP] && this.body[this.body.length - 1].dir != Direction.DOWN)
		{
			this.vel.x = 0; // para de mover horizontalmente
			this.vel.y = -1;  // e move verticalmente a cabeça
			this.body[this.body.length - 1].dir = Direction.UP;
		}
		else if (this.keys[Direction.DOWN] && this.body[this.body.length - 1].dir != Direction.UP)
		{
			this.vel.x = 0; // para de mover horizontalmente
			this.vel.y = 1; // e move verticalmente a cabeça
			this.body[this.body.length - 1].dir = Direction.DOWN;
		}

		// depois ajustando as posições das outras peças (partes)
		// primeiro move as partes do corpo
		if (this.vel.x != 0 || this.vel.y != 0) // se estiver movendo
		{
			for (let i = 0; i < this.body.length - 1; i++)
			{
				// faça a peça de trás (pedaco[i]) igual a peça da frente (pedaco[i + 1])
				this.body[i].pos.x =this.body[i + 1].pos.x;
				this.body[i].pos.y =this.body[i + 1].pos.y;
				this.body[i].dir =this.body[i + 1].dir;
				this.body[i].setPos(this.body[i].pos.x, this.body[i].pos.y, this.pixelSize);
			}
		}
		// agora move a cabeça
		this.body[this.body.length - 1].pos.x += this.vel.x;
		this.body[this.body.length - 1].pos.y += this.vel.y;
		this.body[this.body.length - 1].setPos(this.body[this.body.length - 1].pos.x, this.body[this.body.length - 1].pos.y, this.pixelSize);

		// Verifica os limites do movimento da cobra
		// Para o eixo X
		// se estiver além da largura da tela/mapa
		if (this.body[this.body.length - 1].pos.x >= this.mapWidth +this.mapStartW)
		{
			// volte para posição coorX = 0
			this.body[this.body.length - 1].pos.x =this.mapStartW;
		}
		else if (this.body[this.body.length - 1].pos.x <this.mapStartW) // se estiver além de 0
		{
			// volte para a posição da largura do mapa
			this.body[this.body.length - 1].pos.x =this.mapStartW + this.mapWidth - 1;
		}

		// Para o eixo Y
		if (this.body[this.body.length - 1].pos.y >= this.mapHeight +this.mapStartH)
		{
			this.body[this.body.length - 1].pos.y =this.mapStartH;
		}
		else if (this.body[this.body.length - 1].pos.y <this.mapStartH)
		{
			this.body[this.body.length - 1].pos.y =this.mapStartH + this.mapHeight - 1;
		}

		if (this.vel.x != 0 || this.vel.y != 0)
			Events.instance().emit('snake.move', this);
	}
		
	// se é uma parte com comiddlea
	isEaten ( p: Piece ): boolean
	{
		for (let q of this.eatenPos)
		{
			if (q.x == p.pos.x && q.y == p.pos.y)
				return true;
		}
		
		return false;
	}
	
	updateBody (  )
	{
		//ponta do rabo
		if (this.body[0].dir == Direction.UP)
			this.body[0].sprite.loadTexture("tailRegion0");
		else if (this.body[0].dir == Direction.RIGHT)
			this.body[0].sprite.loadTexture("tailRegion1");
		else if (this.body[0].dir == Direction.DOWN)
			this.body[0].sprite.loadTexture("tailRegion2");
		else if (this.body[0].dir == Direction.LEFT)
			this.body[0].sprite.loadTexture("tailRegion3");
				
		//corpo
		for (var i = 1; i < this.body.length - 1; ++i)
		{
			let before = this.body[i-1].dir;
			let middle = this.body[i].dir;
			let after = this.body[i+1].dir;
			let eaten = this.isEaten(this.body[i]);
			this.body[i].updateSprite(before, middle, after, this.pixelSize, eaten);
		}

		//cabeça
		if (this.body[this.body.length - 1].dir == Direction.UP)
			this.body[this.body.length - 1].sprite.loadTexture("headRegion1");
		else if (this.body[this.body.length - 1].dir == Direction.RIGHT)
			this.body[this.body.length - 1].sprite.loadTexture("headRegion2");
		else if (this.body[this.body.length - 1].dir == Direction.DOWN)
			this.body[this.body.length - 1].sprite.loadTexture("headRegion3");
		else if (this.body[this.body.length - 1].dir == Direction.LEFT)
			this.body[this.body.length - 1].sprite.loadTexture("headRegion0");
	}
	
	
	startGame (  )
	{
		//tamanho_anterior = tamanho; // para o hud
		//Reinicie o jogo
		//tamanho = 5;
		this.eatSnake = false;
		this.vel.x = 0;
		this.vel.y = 0;
		
		for (let i = 0; i < this.keys.length; i++)
			this.keys[i] = false;
		
		
		// reinicializando as peças
		// inicializando a parte A - a cabeça
		while (this.body.length > 0)
			this.body.pop().destroy();
		for (let i = 5; i > 0; i--)
		{
			this.body.push(new Piece(Math.floor(this.mapWidth/2) + this.mapStartW, Math.floor(this.mapHeight/2) + i + this.mapStartH, 
				Direction.UP, 
			 {
				 game:this.game, key:"apple", 
				 width:this.pixelSize, height: this.pixelSize
			 }));
		}
		this.updateBody();
		Events.instance().emit("snake.body.growup", this);
		// inicializando as coordenadas da maçã.
		//posiciona_maca();
		for (let p of this.apples)
			this.setApplePos(p);
		Events.instance().emit("snake.start.game", this);
	}
	
	setApplePos ( apple: Piece )
	{
		if (!apple)
			return;
		
		let repeat = false;
		let applePos = [0,0];
		let appleRect = new Phaser.Rectangle(0,0,this.pixelSize,this.pixelSize);
		do
		{
			// escolhe aleatoriamente as coordenadas
			applePos[0] = Math.floor((Math.random() * (this.mapWidth) + this.mapStartW + 1));
			applePos[1] = Math.floor((Math.random() * (this.mapHeight) + this.mapStartH + 1));
	
			repeat = false;
			
			if (applePos[0] >= this.mapWidth +this.mapStartW - 1)
				applePos[0] = (this.mapWidth +this.mapStartW - 2);
			
			if (applePos[1] >= this.mapHeight +this.mapStartH - 1)
				applePos[1] = (this.mapHeight +this.mapStartH - 2);
			
			if (applePos[0] == 1 && applePos[1] == 1)
				repeat = true;
			else if (applePos[0] == this.mapWidth - 2 && applePos[1] == 1)
				repeat = true;
			else if (applePos[0] == this.mapWidth - 2 && applePos[1] == this.mapHeight - 2)
				repeat = true;
			else if (applePos[0] == 1 && applePos[1] == this.mapHeight - 2)
				repeat = true;

			for (let p of this.body)
			{
				// verifica em todas as peças se as coordenadas delas
				// são iguais as novas coordenadas da maçã.
				if ((p.pos.x == applePos[0]) && (p.pos.y == applePos[1]))
				{
					appleRect.x = Math.floor(applePos[0]*this.pixelSize);
					appleRect.y = Math.floor(applePos[1]*this.pixelSize);
				
					// Se forem iguais então pare o loop e
					// repita o procedimento para escolher outra coordenada para a maçã.
					repeat = true;
					break;
				}
			}
			
			for (let r of this.excludedArea)
				if (Phaser.Rectangle.intersects(r, appleRect))
				{
					repeat = true;
					break;
				}
		// enquanto for para repetir continue escolhendo outra coordenada para a maçã.
		} while (repeat);

		apple.setPos(applePos[0], applePos[1], this.pixelSize);
	}
	
	//callback
	keyboard ( sender, that )
	{
		let key: Phaser.Key = sender.getPhaserKey();
		switch (key.keyCode)
		{
			case Phaser.Keyboard.UP:
				that.keys[Direction.UP]=true;
				that.keys[Direction.DOWN]=false;
				that.keys[Direction.RIGHT]=false;
				that.keys[Direction.LEFT]=false;
				break;
			case Phaser.Keyboard.DOWN:
				that.keys[Direction.UP]=false;
				that.keys[Direction.DOWN]=true;
				that.keys[Direction.RIGHT]=false;
				that.keys[Direction.LEFT]=false;
				break;
			case Phaser.Keyboard.RIGHT:
				that.keys[Direction.UP]=false;
				that.keys[Direction.DOWN]=false;
				that.keys[Direction.RIGHT]=true;
				that.keys[Direction.LEFT]=false;
				break;
			case Phaser.Keyboard.LEFT:
				that.keys[Direction.UP]=false;
				that.keys[Direction.DOWN]=false;
				that.keys[Direction.RIGHT]=false;
				that.keys[Direction.LEFT]=true;
				break;
			case Phaser.Keyboard.ENTER:
				that.setApplePos(that.apple[0]);
				break;
		}
	}
}
