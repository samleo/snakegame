import Events from '../engine/Events';
export default class PeloadState extends Phaser.State
{
	preload(game: Phaser.Game):void
	{
		let logoSprite = game.add.sprite(game.world.centerX, game.world.centerY, "logo");
		logoSprite.anchor.setTo(0, 0);
		logoSprite.y = game.height * 0.25;
		logoSprite.x = game.world.centerX - logoSprite.width / 2;

		let preloadBar = game.add.sprite(game.world.centerX, game.world.centerY, "preloadBar");
		preloadBar.anchor.setTo(0, 0);
		preloadBar.y = logoSprite.y + logoSprite.height + 1.5 * preloadBar.height;
		preloadBar.x = game.world.centerX - preloadBar.width / 2;

		game.load.setPreloadSprite(preloadBar);

		game.load.bitmapFont(game["fontName"], "assets/fonts/font.png", "assets/fonts/font.xml");
		game.load.image("backgroundMainMenu", "assets/backgroundMainMenu.png");
		game.load.image("mainButtonBG", "assets/mainButtonBG.png");
		game.load.image("backButtonBG", "assets/backButtonBG.png");

		game.load.image("markerOptionNormal", "assets/markerOptionNormal.png");
		game.load.image("markerOptionSelected", "assets/markerOptionSelected.png");

		game.load.image("rock", "assets/snake/rock.png");
		game.load.image("apple", "assets/snake/apple.png"); 
		//this.applesRegion.flip(false, true);

		/////Snakes///////
		game.load.image("lineRegion0", "assets/snake/pieceLine0.png");
		//lineRegion0.flip(false,true);
		game.load.image("lineRegion1", "assets/snake/pieceLine1.png");
		//lineRegion1.flip(false,true);
		game.load.image("cornerRegion0", "assets/snake/pieceCorner0.png");
		//cornerRegion0.flip(false,true);
		game.load.image("cornerRegion1", "assets/snake/pieceCorner1.png");
		//cornerRegion1.flip(false,true);
		game.load.image("cornerRegion2", "assets/snake/pieceCorner2.png");
		//cornerRegion2.flip(false,true);
		game.load.image("cornerRegion3", "assets/snake/pieceCorner3.png");
		//cornerRegion3.flip(false,true);
		
		game.load.image("headRegion0", "assets/snake/head0.png");
		//headRegion0.flip(false,true);
		game.load.image("headRegion1", "assets/snake/head1.png");
		//headRegion1.flip(false,true);
		game.load.image("headRegion2", "assets/snake/head2.png");
		//headRegion2.flip(false,true);
		game.load.image("headRegion3", "assets/snake/head3.png");
		//headRegion3.flip(false,true);
		
		game.load.image("tailRegion0", "assets/snake/tail0.png");
		//tailRegion0.flip(false,true);
		game.load.image("tailRegion1", "assets/snake/tail1.png");
		//tailRegion1.flip(false,true);
		game.load.image("tailRegion2", "assets/snake/tail2.png");
		//tailRegion2.flip(false,true);
		game.load.image("tailRegion3", "assets/snake/tail3.png");
		//tailRegion3.flip(false,true);
		
		game.load.image("eatenLineRegion0", "assets/snake/eaten0.png");
		//eatenLineRegion0.flip(false,true);
		game.load.image("eatenLineRegion1", "assets/snake/eaten1.png");
		//eatenLineRegion1.flip(false,true);
		game.load.image("eatenCornerRegion0", "assets/snake/eatenCorner0.png");
		//eatenCornerRegion0.flip(false,true);
		game.load.image("eatenCornerRegion1", "assets/snake/eatenCorner1.png");
		//eatenCornerRegion1.flip(false,true);
		game.load.image("eatenCornerRegion2", "assets/snake/eatenCorner2.png");
		//eatenCornerRegion2.flip(false,true);
		game.load.image("eatenCornerRegion3","assets/snake/eatenCorner3.png");
		//eatenCornerRegion3.flip(false,true);

		game.load.image("background", "assets/snake/bg.png");
		game.load.image('controls', "assets/controls.png");
	}

	create(game: Phaser.Game): void
	{
		game.state.start("MainMenu");
	}

	shutdown(game)
	{
		Events.instance().destroy();
		super.shutdown(game);
	}
}