
export enum Direction
{
	UP = 0,
	DOWN = 1,
	RIGHT = 2,
	LEFT = 3,
}

export default class Piece 
{
	pos: Phaser.Point;
	dir: number;
	sprite: Phaser.Sprite;
	
	constructor ( x: number, y: number, d: number, config: any = null ) {
		this.pos = new Phaser.Point(x, y);
		this.dir = d;
		if (config)
		{
			this.sprite = new Phaser.Sprite(config.game, x * config.width, y * config.height, config.key);
			this.sprite.game.add.existing(this.sprite);
			this.sprite.width = config.width;
			this.sprite.height = config.height;
		}
	}

	destroy ()
	{
		if (this.sprite)
			this.sprite.destroy();
	}

	setPos (x, y, pixelSize)
	{
		this.pos.x = x;
		this.pos.y = y;
		if (!this.sprite)
			return;
		this.sprite.x = x * pixelSize;
		this.sprite.y = y * pixelSize;
	}
	
	updateSprite ( before: number, middle: number, after: number, pixelSize: number, isEaten: boolean )
	{
		if (!this.sprite)
		{
			alert("Sprite indefinido");
			return;
		}

		let key = this.sprite.key;
		this.sprite.x = this.pos.x * pixelSize;
		this.sprite.y = this.pos.y * pixelSize;
		/*
			* O da frente não pode estar na direção contraria ao de trás
			*/
		if ((before == Direction.LEFT && middle == Direction.LEFT && after == Direction.LEFT) ||
				(before == Direction.LEFT && middle == Direction.LEFT && after == Direction.UP) ||
				(before == Direction.LEFT && middle == Direction.LEFT && after == Direction.DOWN))
		{
			if (isEaten == false)
			//batch.draw(lineRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "lineRegion1";
			else
				//batch.draw(eatenLineRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenLineRegion1";
		}
		//else if (before == Direction.LEFT && middle == Direction.RIGHT && after == Direction.LEFT) //não pode
		else if ((before == Direction.LEFT && middle == Direction.DOWN && after == Direction.LEFT) || 
							(before == Direction.LEFT && middle == Direction.DOWN && after == Direction.RIGHT) ||
							(before == Direction.LEFT && middle == Direction.DOWN && after == Direction.DOWN))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion3,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion3";
			else
				//batch.draw(eatenCornerRegion3,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion3";
		}
		else if ((before == Direction.LEFT && middle == Direction.UP && after == Direction.LEFT) || 
							(before == Direction.LEFT && middle == Direction.UP && after == Direction.RIGHT) ||
							(before == Direction.LEFT && middle == Direction.UP && after == Direction.UP))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion2,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion2";
			else
				//batch.draw(eatenCornerRegion2,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion2";
		}
		//else if ((before == Direction.RIGHT && middle == Direction.LEFT && after == Direction.LEFT)//não pode
		else if ((before == Direction.RIGHT && middle == Direction.RIGHT && after == Direction.DOWN) || 
							(before == Direction.RIGHT && middle == Direction.RIGHT && after == Direction.RIGHT) ||
							(before == Direction.RIGHT && middle == Direction.RIGHT && after == Direction.UP))
		{
			if (isEaten == false)
				//batch.draw(lineRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "lineRegion1";
			else
				//batch.draw(eatenLineRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenLineRegion1";
		}
		else if ((before == Direction.RIGHT && middle == Direction.DOWN && after == Direction.DOWN) || 
							(before == Direction.RIGHT && middle == Direction.DOWN && after == Direction.RIGHT) ||
							(before == Direction.RIGHT && middle == Direction.DOWN && after == Direction.LEFT))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion0";
			else
				//batch.draw(eatenCornerRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion0";
		}
		else if ((before == Direction.RIGHT && middle == Direction.UP && after == Direction.UP) || 
							(before == Direction.RIGHT && middle == Direction.UP && after == Direction.RIGHT) ||
							(before == Direction.RIGHT && middle == Direction.UP && after == Direction.LEFT))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion1";
			else
				//batch.draw(eatenCornerRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion1";
		}
		else if ((before == Direction.UP && middle == Direction.UP && after == Direction.UP) || 
							(before == Direction.UP && middle == Direction.UP && after == Direction.RIGHT) ||
							(before == Direction.UP && middle == Direction.UP && after == Direction.LEFT))
		{
			if (isEaten == false)
				//batch.draw(lineRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "lineRegion0";
			else
				//batch.draw(eatenLineRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenLineRegion0";
		}
		else if ((before == Direction.UP && middle == Direction.RIGHT && after == Direction.UP) || 
							(before == Direction.UP && middle == Direction.RIGHT && after == Direction.RIGHT) ||
							(before == Direction.UP && middle == Direction.RIGHT && after == Direction.DOWN))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion3,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion3";
			else
				//NOTA: será bug aqui? são iguais
				//batch.draw(cornerRegion3,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion3";
		}
		else if ((before == Direction.UP && middle == Direction.LEFT && after == Direction.UP) || 
							(before == Direction.UP && middle == Direction.LEFT && after == Direction.DOWN) ||
							(before == Direction.UP && middle == Direction.LEFT && after == Direction.LEFT))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion0";
			else
				//batch.draw(eatenCornerRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion0";
		}
		else if ((before == Direction.DOWN && middle == Direction.LEFT && after == Direction.UP) || 
							(before == Direction.DOWN && middle == Direction.LEFT && after == Direction.DOWN) ||
							(before == Direction.DOWN && middle == Direction.LEFT && after == Direction.LEFT))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion1";
			else
				//batch.draw(eatenCornerRegion1,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion1";
		}
		else if ((before == Direction.DOWN && middle == Direction.RIGHT && after == Direction.UP) || 
							(before == Direction.DOWN && middle == Direction.RIGHT && after == Direction.RIGHT) ||
							(before == Direction.DOWN && middle == Direction.RIGHT && after == Direction.DOWN))
		{
			if (isEaten == false)
				//batch.draw(cornerRegion2,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "cornerRegion2";
			else
				//batch.draw(eatenCornerRegion2,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenCornerRegion2";
		}
		else if ((before == Direction.DOWN && middle == Direction.DOWN && after == Direction.LEFT) || 
							(before == Direction.DOWN && middle == Direction.DOWN && after == Direction.RIGHT) ||
							(before == Direction.DOWN && middle == Direction.DOWN && after == Direction.DOWN))
		{
			if (isEaten == false)
				//batch.draw(lineRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "lineRegion0";
			else
				//batch.draw(eatenLineRegion0,dest.x,dest.y,this.pixelSize,this.pixelSize);
				key = "eatenLineRegion0";
		}
		//else if ((before == Direction.DOWN && middle == Direction.UP && after == Direction.UP)//não pode 

		if (key != this.sprite.key)
			this.sprite.loadTexture(key);
	}

	randomPos ( mapWidth: number, mapHeight: number )
	{
		this.pos.x = Math.floor(Math.random() * mapWidth);
		this.pos.y = Math.floor(Math.random() * mapHeight);
	}
}