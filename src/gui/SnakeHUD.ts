import Button from './Button';
import Events from '../engine/Events';
/**
 * Sinais emitidos:
 * "snakehud.to.mainmenu"
 */

export default class SnakeHUD extends Phaser.BitmapText
{
	currentSize: number;
	sizeText: Phaser.BitmapText;
	back: Button;
	controls: Phaser.Sprite;
	
	constructor (config)
	{
		super(config.game, config.x, config.y, config.font.key);
		this.anchor.set(0,0);

		this.back = new Button({game: config.game, x:0, y: 0, key: "backButtonBG"});
		this.back.x = this.game.width - this.back.width;

		this.sizeText = new Phaser.BitmapText(config.game, 0, 0, config.font.key, "Score: 0", config.font.size);
		this.addChild(this.sizeText);
		this.addChild(this.back);
		this.game.add.existing(this);

		let that = this;

		this.controls = new Phaser.Sprite(config.game, 0, 0, 'controls');
		this.addChild(this.controls);
		config.game.add.existing(this.controls);
		this.controls.z = 10000;
		Events.instance().on("snake.body.growup", function(snake, that)
		{
			if (that.game["scores"])
			{
				if (that.game["scores"] < snake.body.length)
					that.game["scores"] = snake.body.length;
			}
			else
				that.game["scores"] = snake.body.length;
			that.currentSize = snake.body.length;
			that.sizeText.text = "Score: " + String(that.currentSize);
		}, this);

		Events.instance().on('snake.start.game', function(){
			that.controls.visible = true;
		}, this);
		Events.instance().on('snake.move', function(){
			that.controls.visible = false;
		}, this);

		this.back.onInputUp.add(function()
		{
			Events.instance().emit("snake.hud.click.back", that);
			that.game.state.start("MainMenu",false, false);
		});
	}
}