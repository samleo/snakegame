export default class Boot extends Phaser.State
{
	preload(game: Phaser.Game):void
	{
		game.load.image("logo", "assets/logo.png"); 
		game.load.image("preloadBar", "assets/preloadBar.png");
	}

	create(game: Phaser.Game): void
	{
		game.state.start("PreloadState");
	}
}