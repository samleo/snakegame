// classe que se compara com a de eventManager do phaser 3

export class Callback
{
	call: Function;
	context: any;
	constructor(callback: Function, context: any)
	{
		this.call = callback;
		this.context = context;
	}
}

export class Event
{
	eventName: string;
	callbacks: Array<Callback>;
	constructor ( eventName: string, callbacks: Array<Callback> )
	{
		this.eventName = eventName;
		this.callbacks = callbacks;
	}

	add ( callback: Callback ): void
	{
		if (this.callbacks.indexOf(callback) == -1)
		{
			this.callbacks.push(callback);
		}
	}

	removeCallback ( context: any ): void
	{
		let counter = 0;
		for (let callback of this.callbacks)
		{
			if (callback.context == context)
			{
				this.callbacks.splice(counter, 1);
				break;
			}
			counter++;
		}
	}

	notify (sender: any): void
	{
		for (let callback of this.callbacks)
			callback.call(sender, callback.context);
	}
}

export default class Events
{
	protected events: Array<Event>;
	protected static singleton: Events = null;
	constructor()
	{
		this.events = Array<Event>();
	}

	public static instance (): Events
	{
		return (this.singleton || (this.singleton = new this()));
	}

	destroy ()
	{
		while (this.events.length > 0)
			this.events.pop();
	}

	on ( eventName: string, callback: Function, context: any = null ): void
	{
		let found = false;
		for (let event of this.events)
			if (event.eventName == eventName)
			{
				let has = false;
				for (let cb of event.callbacks)
					if (cb.context == context)
					{
						has = true;
						break;
					}
				
					if (has == false)
					{
						event.add(new Callback(callback, context))
					}
				found = true;
			}
		
		if (found == false)
			this.events.push(new Event(eventName, [new Callback(callback, context)]));
	}

	off ( eventName: string, context: any ): void
	{
		for (let event of this.events)
			if (event.eventName == eventName)
			{
				event.removeCallback(context);
				break;
			}
	}

	emit ( eventName: string, sender: any ): void
	{
		for (let event of this.events)
			if (event.eventName == eventName)
				event.notify(sender);
	}
}