/**
 * Sinais emitidos:
 * key.state.free
 * key.state.press
 * key.state.hold
 * key.state.release
 */
import StateMachine from "./StateMachine";
import Events from './Events';

export enum KeyState
{
	FREE,
	PRESS,
	HOLD,
	RELEASE,
}

export default class Key extends StateMachine
{
	public pressed: boolean;
	private key: Phaser.Key;

	constructor (game: Phaser.Game, keyCode: number)
	{
		super();
		this.key = game.input.keyboard.addKey(keyCode);
		this.key.enabled = true;
		this.pressed = false;
		this.setState(KeyState.FREE);
		Events.instance().emit("ley.state.free", this);
	}

	getPhaserKey (  ): Phaser.Key
	{
		return this.key;
	}

	setPhaserKey ( key: Phaser.Key ): void
	{
		this.key = key;
	}

	updateInput ( event: any = null ): void
	{
		this.pressed = this.key.isDown;
		Events.instance().emit("key.press", this);
	}

	updateState (): number
	{
		switch (this.getState())
		{
			case KeyState.FREE:
				if (this.pressed)
				{
					this.setState(KeyState.PRESS);
					Events.instance().emit("key.state.press", this);
				}
				break;
			
			case KeyState.PRESS:
				if (this.pressed)
				{
					this.setState(KeyState.HOLD);
					Events.instance().emit("key.state.hold", this);
				}
				else
				{
					this.setState(KeyState.RELEASE);
					Events.instance().emit("key.state.release", this);
				}
				break;
			
			case KeyState.HOLD:
				if (this.pressed == false)
				{
					this.setState(KeyState.RELEASE);
					Events.instance().emit("key.state.release", this);
				}
				break;
			
			case KeyState.RELEASE:
				this.pressed = false;
				this.setState(KeyState.FREE);
				Events.instance().emit("key.state.free", this);
				break;
			
			default:
				this.setState(KeyState.FREE);
				break;
		}

		return this.getState();
	}
}