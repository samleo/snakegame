import Events from '../engine/Events';
export default class Marker extends Phaser.Button
{
	protected state: boolean;
	protected imageSelected: Phaser.Image;
	protected background: Phaser.Image;
	
	constructor (config)
	{
		super(config.game, config.x, config.y, null);
		this.game.add.existing(this);
		this.anchor.set(0,0);
		this.background = new Phaser.Image(config.game, 0,0,config.normal);
		this.imageSelected = new Phaser.Image(config.game, 0,0,config.selected);
		this.state = false;
		
		this.addChild(this.background);
		this.addChild(this.imageSelected);
		this.imageSelected.visible = false;
		if (config.name)
			this.name = config.name;
		
		let that = this;
		this.onInputUp.add(function()
		{
			that.select(that.isSelected() == false);	
		});

		Events.instance().emit("marker.select.false", this);
	}

	select (s: boolean)
	{
		this.state = s;
		if (s)
		{
			this.background.visible = false;
			this.imageSelected.visible = true;
			Events.instance().emit("marker.select.true", this);
		}
		else
		{
			this.background.visible = true;
			this.imageSelected.visible = false;
			Events.instance().emit("marker.select.false", this);
		}
	}

	isSelected (): boolean
	{
		return this.state;
	}
}